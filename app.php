#!/usr/bin/env php
<?php

require __DIR__.'/vendor/autoload.php';

use FixSuggestions\Command\GenerateFixSuggestions;
use FixSuggestions\Command\PostFixSuggestions;
use Symfony\Component\Console\Application;

$application = new Application();
$application->addCommands( [
	new GenerateFixSuggestions(),
    new PostFixSuggestions()
] );
$application->run();
