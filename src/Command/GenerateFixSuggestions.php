<?php

namespace FixSuggestions\Command;

use FixSuggestions\Generator\Gerrit;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateFixSuggestions extends Command {

	public function configure(): void {
		$this->setName( 'suggestions:generate' );
		$this->setDescription( 'Generate fix suggestions for a patch.' );
		$this->addOption(
			'suggestion-tools',
			null,
			InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY,
			'Tools to use for generating fix suggestions.',
			$this->defaultTools()
		);
		$this->addOption( 'working-directory',
			null,
			InputOption::VALUE_OPTIONAL,
			'Working directory where fix suggestion tools should be run.',
			getcwd()
		);
		$this->addOption( 'files',
		null,
		InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY,
		'Files/directories to pass as arguments to the fix suggestion tools',
			[ getcwd() ]
		);
		$this->addOption(
			'output-directory',
			null,
			InputOption::VALUE_OPTIONAL,
			'Path to where the fix suggestion output should be saved.',
			getcwd() . '/reports'
		);
		$this->addOption(
			'project',
			null,
			InputOption::VALUE_OPTIONAL,
			'Project name. Used to organizing cache and report directories.',
			'unknown'
		);
		$this->addOption(
			'cache-directory',
			null,
			InputOption::VALUE_OPTIONAL,
			'Path to where phpcs runs are cached.',
			getcwd() . '/cache'
		);
		$this->addOption(
			'code-review',
			null,
			InputOption::VALUE_REQUIRED,
			'Code review system that suggestions will be generated for',
			$this->defaultCodeReview()
		);
		$this->addOption(
			'gerrit-robot-id',
			null,
			InputOption::VALUE_OPTIONAL,
			'[gerrit only] The robot ID to use in fix suggestions.',
			getenv( 'GERRIT_ROBOT_ID' )
		);
		$this->addOption(
			'gerrit-robot-run-id',
			null,
			InputOption::VALUE_OPTIONAL,
			'[gerrit only] The robot run ID to use in fix suggestions.',
			getenv( 'GERRIT_ROBOT_RUN_ID' )
		);
		$this->addOption(
			'gerrit-robot-url',
			null,
			InputOption::VALUE_OPTIONAL,
			'[gerrit only] The robot URL to use in fix suggestions.',
			// Ideally would be GERRIT_ROBOT_URL but the report format uses GERRIT_URL so keep
			// that for consistency.
			getenv( 'GERRIT_URL' )
		);
		$this->addOption(
			'format',
			null,
			InputOption::VALUE_OPTIONAL,
			<<<'TAG'
The output format to return for the command. Default is a JSON object 
containing information about generated reports.
TAG
,
			'json'
		);
	}

	/** @inheritDoc */
	public function execute( InputInterface $input, OutputInterface $output ): int {
		$validateTools = array_diff( $input->getOption( 'suggestion-tools' ), $this->supportedTools() );
		if ( count( $validateTools ) ) {
			$output->writeln(
				sprintf( '<error>Unsupported tools specified:</error> %s', implode( ', ', $validateTools ) )
			);
			return Command::FAILURE;
		}

		if ( !in_array( $input->getOption( 'code-review' ), $this->supportedCodeReview() ) ) {
			$output->writeln(
				sprintf( '<error>Unsupported code review system specified:</error> %s',
					implode( ', ', $input->getOption( 'code-review' ) ) )
			);
			return Command::FAILURE;
		}

		// TODO: Maybe extract this block into a class once we have GitLab and/or eslint added.
		if ( $input->getOption( 'code-review' ) === Gerrit::ID ) {
			$cacheFile = sprintf( '%s/%s.json',
				$input->getOption( 'cache-directory' ),
				$input->getOption( 'project' ) );
			$generator = new Gerrit(
				$cacheFile,
				$input->getOption( 'files' ),
				$input->getOption( 'working-directory' ),
				$input->getOption( 'gerrit-robot-id' ),
				$input->getOption( 'gerrit-robot-run-id' ),
				$input->getOption( 'gerrit-robot-url' ),
				$input->getOption( 'output-directory' ),
				$input->getOption( 'project' )
			);
			$generator->setTools( $input->getOption( 'suggestion-tools' ) );
			$response = $generator->execute();
			if ( $input->getOption( 'format' ) === 'json' ) {
				$output->write( json_encode( $response ) );
			}
			return Command::SUCCESS;
		}

		return Command::SUCCESS;
	}

	private function supportedTools(): array {
		return [ 'phpcs' ];
	}

	private function defaultTools(): array {
		return [ 'phpcs' ];
	}

	private function supportedCodeReview(): array {
		return [ Gerrit::ID ];
	}

	private function defaultCodeReview(): array {
		return [ Gerrit::ID ];
	}

}
