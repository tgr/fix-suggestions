<?php

namespace FixSuggestions\Generator;

use Symfony\Component\Process\Process;

class Gerrit {

	public const ID = 'gerrit';

	/** @var string[] */
	private $tools;
	/** @var string */
	private $cacheFile;
	/** @var array */
	private $files;
	/** @var string */
	private $workingDirectory;
	/** @var string */
	private $gerritRobotId;
	/** @var string */
	private $gerritRobotRunId;
	/** @var string */
	private $gerritRobotUrl;
	/** @var string */
	private $outputDirectory;
	/** @var string */
	private $project;

	/**
	 * @param string $cacheFile
	 * @param string[] $files
	 * @param string $workingDirectory
	 * @param string $gerritRobotId
	 * @param string $gerritRobotRunId
	 * @param string $gerritRobotUrl
	 * @param string $outputDirectory
	 * @param string $project
	 */
	public function __construct(
		string $cacheFile,
		array $files,
		string $workingDirectory,
		string $gerritRobotId,
		string $gerritRobotRunId,
		string $gerritRobotUrl,
		string $outputDirectory,
		string $project
	) {
		$this->cacheFile = $cacheFile;
		$this->files = $files;
		$this->workingDirectory = $workingDirectory;
		$this->gerritRobotId = $gerritRobotId;
		$this->gerritRobotRunId = $gerritRobotRunId;
		$this->gerritRobotUrl = $gerritRobotUrl;
		$this->outputDirectory = $outputDirectory;
		$this->project = $project;
	}

	/**
	 * @param array $tools
	 */
	public function setTools( array $tools ) {
		$this->tools = $tools;
	}

	public function execute(): array {
		$this->makeDirectoriesIfNeeded( $this->cacheFile );
		$response = [];
		if ( $this->shouldRunPhpcs() ) {
			$phpcsCommand = [
				'vendor/bin/phpcs',
				'--cache=' . $this->cacheFile,
				'-q',
				'--report=vendor/mediawiki/mediawiki-codesniffer/MediaWiki/Reports/GerritRobotComments.php'
			];
			$process = new Process(
				array_merge( $phpcsCommand, $this->files ),
				$this->workingDirectory,
				[
					'GERRIT_ROBOT_ID' => $this->gerritRobotId,
					'GERRIT_ROBOT_RUN_ID' => $this->gerritRobotRunId,
					'GERRIT_URL' => $this->gerritRobotUrl,
					'PHPCS_ROOT_DIR' => $this->workingDirectory
				]
			);
			$process->run();
			// No lint errors, return early.
			if ( $process->getExitCode() === 0 ) {
				return $response;
			}
			if ( $process->getExitCode() !== 2 ) {
				// phpcs returns exit code 2 even when generating report properly.
				throw new \Exception( $process->getErrorOutput() . "\n" . $process->getOutput() );
			}
			$outputFile = sprintf( '%s/%s/%s/phpcs.%s.json',
				$this->outputDirectory,
				self::ID,
				$this->project,
				$this->gerritRobotRunId
			);
			$this->makeDirectoriesIfNeeded( $outputFile );
			file_put_contents(
				$outputFile,
				$process->getOutput()
			);
			$response['code_review'] = self::ID;
			$response['suggestions']['phpcs'] = $outputFile;
		}

		return $response;
	}

	/**
	 * @param string $filename
	 */
	private function makeDirectoriesIfNeeded( string $filename ): void {
		if ( !file_exists( dirname( $filename ) ) ) {
			mkdir( dirname( $filename ), 0777, true );
		}
	}

	private function shouldRunPhpcs(): bool {
		return in_array( 'phpcs', $this->tools );
	}
}
